﻿using Medacademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medacademy.Repository
{
    public class PaginationRepository
    {
       
        // Passing TotalItems And Divide Number
        public PaginationModel CalculatePage(PaginationModel model)
        {
            float TotalItems = model.TotalItems;
            float divid = model.DividNumber;
            string calcilateItems = Convert.ToString(TotalItems / divid);
            int reslts;
            bool res = Int32.TryParse(calcilateItems, out reslts);
            if (res == true)
            {
                model.NumberOfPages = Convert.ToInt32(calcilateItems);
            }
            else
            {
                string[] a1 = calcilateItems.Split('.');
                int a2 = Convert.ToInt32(a1[0]);
                model.NumberOfPages = Convert.ToInt32(a2 + 1);
            }
            return model;
        }

    }
}