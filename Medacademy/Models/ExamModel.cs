﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Medacademy.Models
{
    public class ExamModel
    {
        public long ExamID { get; set; }

        [Required(ErrorMessage = "Group Required")]
        public string GroupID { get; set; }

        [Required(ErrorMessage = "LevelName Required")]
        public string ExamName { get; set; }
        [Required(ErrorMessage = "Duration Required")]
        public int Duration { get; set; }
        [Required(ErrorMessage = "StartDate Required")]
        public DateTime StartDate { get; set; }
        [Required(ErrorMessage = "EndDate Required")]
        public DateTime EndDate { get; set; }
        public bool IsActive { get; set; }

        public bool Upcomingstatus { get; set; }
        public bool PublishStatus { get; set; }
        public System.DateTime Timestamp { get; set; }
    }
}