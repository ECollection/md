﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medacademy.Models;
using Ma.ClassLibrary;
using Ma.EntityLibrary;
using System.IO;
using Ma.EntityLibrary.Data;

namespace Medacademy.Controllers
{
    public class AdminController : Controller
    {
        public DateTime CurrentTime = TimeZoneInfo.ConvertTimeFromUtc(System.DateTime.Now.ToUniversalTime(), TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
        public MAEntities _Entities = new MAEntities();
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Dashboard()
        {
            return View();
        }
        public ActionResult Logout()
        {
            Session.Abandon();
            // FormsAuthentication.SignOut();
            return RedirectToAction("../Accounts/Index");
        }

        #region Course
        public ActionResult Course()
        {
            return View();
        }
        public PartialViewResult AddCourse()
        {
            return PartialView("~/Views/Admin/_pv_Add_Course.cshtml", new Medacademy.Models.CourseModel());
        }
        [HttpPost]
        public object AddCourse(CourseModel model)
        {
            bool status = false;
            string msg = string.Empty;
            //if (ModelState.IsValid)
            //{
            var Courses = _Entities.tb_Course.Where(z => z.CourseName.Trim() == model.CourseName.Trim() && z.IsActive).ToList();
            if (Courses.Count == 0)
            {
                var cls = new tb_Course();
                cls.CourseName = model.CourseName;
                cls.CourseSubjectName = model.CourseSubjectName;
                cls.IsActive = true;
                cls.TimeStamp = CurrentTime;
                cls.Price = model.Price;
                cls.Duration = model.Duration;
                cls.Details = model.Details;

                //Course Pic
                if (model.Courseimage != null)
                {
                    string folderPath = Server.MapPath("~/Files/CourseImage/");
                    if (!Directory.Exists(folderPath))
                        Directory.CreateDirectory(folderPath);
                    var imageString = model.Courseimage.Substring(model.Courseimage.IndexOf(',') + 1);
                    byte[] imageByte = Convert.FromBase64String(imageString);
                    string imageName = Guid.NewGuid().ToString() + ".jpeg";
                    var imgFilePath = Server.MapPath("~/Files/CourseImage/" + imageName);
                    var fileSave = "/Files/CourseImage/" + imageName;

                    using (var imageFile = new FileStream(imgFilePath, FileMode.Create))
                    {
                        imageFile.Write(imageByte, 0, imageByte.Length);
                        imageFile.Flush();
                        cls.Files = fileSave;
                    }
                }


                //Course Pic
                _Entities.tb_Course.Add(cls);
                status = _Entities.SaveChanges() > 0;
                msg = status ? "Course added successfully!" : "Failed to add Course!";



            }
            else
            {
                status = false;
                msg = "Course already exists!";
            }
            //}
            //else
            //{
            //    status = false;
            //    msg = "Failed to add Course!";
            //}
            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult RefreshCourse()
        {
            return PartialView("~/Views/Admin/_pv_Course_Grid.cshtml", new Medacademy.Models.CourseModel());
        }
        public PartialViewResult EditCourseView(long courseid)
        {
            var classes = _Entities.tb_Course.Where(z => z.CourseId == courseid && z.IsActive).FirstOrDefault();
            var model = new Medacademy.Models.CourseModel();
            model.CourseId = classes.CourseId;
            model.CourseName = classes.CourseName;
            model.CourseSubjectName = classes.CourseSubjectName;
            model.Price = classes.Price;
            model.Details = classes.Details;
            model.Duration = classes.Duration;
            model.Courseimage = classes.Files;

            return PartialView("~/Views/Admin/_pv_Course_EditCourse.cshtml", model);
        }
        [HttpPost]
        public object EditCourse(CourseModel model)
        {
            bool status = false;
            string msg = string.Empty;
            var cls = _Entities.tb_Course.FirstOrDefault(z => z.CourseId == model.CourseId && z.IsActive);
            if (cls != null)
            {

                cls.CourseName = model.CourseName;
                cls.CourseSubjectName = model.CourseSubjectName;
                // cls.IsActive = true;
                // cls.TimeStamp = CurrentTime;
                cls.Price = model.Price;
                cls.Duration = model.Duration;
                cls.Details = model.Details;

                //Course Pic
                if (model.Courseeditimage != null)
                {
                    string folderPath = Server.MapPath("~/Files/CourseImage/");
                    if (!Directory.Exists(folderPath))
                        Directory.CreateDirectory(folderPath);
                    var imageString = model.Courseeditimage.Substring(model.Courseeditimage.IndexOf(',') + 1);
                    byte[] imageByte = Convert.FromBase64String(imageString);
                    string imageName = Guid.NewGuid().ToString() + ".jpeg";
                    var imgFilePath = Server.MapPath("~/Files/CourseImage/" + imageName);
                    var fileSave = "/Files/CourseImage/" + imageName;

                    using (var imageFile = new FileStream(imgFilePath, FileMode.Create))
                    {
                        imageFile.Write(imageByte, 0, imageByte.Length);
                        imageFile.Flush();
                        cls.Files = fileSave;
                    }
                }
                //Course Pic

                status = _Entities.SaveChanges() > 0 ? true : false;
                msg = status ? "Course Updated successfully!" : "Failed to Updated Course!";
            }
            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public object CourseDelete(long courseid)
        {
            bool status = false;
            string msg = string.Empty;
            var courses = _Entities.tb_Course.Where(z => z.CourseId == courseid && z.IsActive).FirstOrDefault();
            if (courses != null)
            {
                courses.IsActive = false;
                status = _Entities.SaveChanges() > 0;
                msg = status ? " Course Deleted!" : "Failed to delete!";
            }
            else
            {
                status = false;
                msg = "Failed to delete!";
            }
            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult CourseView(long courseid)
        {
            var classes = _Entities.tb_Course.Where(z => z.CourseId == courseid && z.IsActive).FirstOrDefault();
            var model = new Medacademy.Models.CourseModel();
            model.CourseId = classes.CourseId;
            model.CourseName = classes.CourseName;
            model.CourseSubjectName = classes.CourseSubjectName;
            model.Price = classes.Price;
            model.Details = classes.Details;
            model.Duration = classes.Duration;
            model.Courseimage = classes.Files;
            model.Isactive = classes.IsActive;

            return PartialView("~/Views/Admin/_pv_Course_View.cshtml", model);
        }

        #endregion

        #region Package
        public ActionResult Package()
        {
            return View();
        }
        public ActionResult AddPackage()
        {
            return View();
        }
        [HttpPost]
        public object AddPackage(PackageModel model)
        {
            bool status = false;
            string msg = string.Empty;
            //if (ModelState.IsValid)
            //{
            var Packages = _Entities.tb_Package.Where(z => z.Name.Trim() == model.PackageName.Trim() && z.Isactive).ToList();
            if (Packages.Count == 0)
            {
                var cls = new tb_Package();
                cls.CourseID = model.CourseID;
                cls.Name = model.PackageName;
                cls.Type = model.Type;
                cls.Amount = model.Amount;
                cls.DiscountAmount = model.DiscountAmount;
                cls.ExpiryDays = model.ExpiryDays;
                cls.Description = model.Description;
                cls.Isactive = true;
                cls.TimeStamp = CurrentTime;


                //Course Pic
                if (model.Packageimage != null)
                {
                    string folderPath = Server.MapPath("~/Files/PackageImage/");
                    if (!Directory.Exists(folderPath))
                        Directory.CreateDirectory(folderPath);
                    var imageString = model.Packageimage.Substring(model.Packageimage.IndexOf(',') + 1);
                    byte[] imageByte = Convert.FromBase64String(imageString);
                    string imageName = Guid.NewGuid().ToString() + ".jpeg";
                    var imgFilePath = Server.MapPath("~/Files/PackageImage/" + imageName);
                    var fileSave = "/Files/PackageImage/" + imageName;

                    using (var imageFile = new FileStream(imgFilePath, FileMode.Create))
                    {
                        imageFile.Write(imageByte, 0, imageByte.Length);
                        imageFile.Flush();
                        cls.Files = fileSave;
                    }
                }


                //Course Pic
                _Entities.tb_Package.Add(cls);
                status = _Entities.SaveChanges() > 0;
                msg = status ? "Package added successfully!" : "Failed to add Package!";



            }
            else
            {
                status = false;
                msg = "Package already exists!";

            }
            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        //else
        //{
        //    status = false;
        //    msg = "Failed to add Package!";
        //}

        public PartialViewResult RefreshPackage()
        {
            return PartialView("~/Views/Admin/_pv_Package_Grid.cshtml", new Medacademy.Models.PackageModel());
        }
        public object PackageDelete(long packageid)
        {
            bool status = false;
            string msg = string.Empty;
            var packages = _Entities.tb_Package.Where(z => z.PackageID == packageid && z.Isactive).FirstOrDefault();
            if (packages != null)
            {
                packages.Isactive = false;
                status = _Entities.SaveChanges() > 0;
                msg = status ? " Package Deleted!" : "Failed to delete!";
            }
            else
            {
                status = false;
                msg = "Failed to delete!";
            }
            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult EditPackageView(long packageid)
        {
            var packages = _Entities.tb_Package.Where(z => z.PackageID == packageid && z.Isactive).FirstOrDefault();
            var model = new Medacademy.Models.PackageModel();
            model.PackageID = packages.PackageID;
            model.CourseID = packages.CourseID;
            model.PackageName = packages.Name;
            model.Type = packages.Type;
            model.Amount = packages.Amount;
            model.DiscountAmount = Convert.ToDecimal(packages.DiscountAmount);
            model.ExpiryDays = packages.ExpiryDays;
            model.Description = packages.Description;
            model.Packageimage = packages.Files;

            return PartialView("~/Views/Admin/_pv_Package_EditPackage.cshtml", model);
        }

        [HttpPost]
        public object EditPackage(PackageModel model)
        {
            bool status = false;
            string msg = string.Empty;
            var cls = _Entities.tb_Package.FirstOrDefault(z => z.PackageID == model.PackageID && z.Isactive);
            if (cls != null)
            {

                cls.CourseID = model.CourseID;
                cls.Name = model.PackageName;
                cls.Type = model.Type;
                cls.Amount = model.Amount;
                cls.DiscountAmount = model.DiscountAmount;
                cls.ExpiryDays = model.ExpiryDays;
                cls.Description = model.Description;


                //Course Pic
                if (model.Packageeditimage != null)
                {
                    string folderPath = Server.MapPath("~/Files/PackageImage/");
                    if (!Directory.Exists(folderPath))
                        Directory.CreateDirectory(folderPath);
                    var imageString = model.Packageeditimage.Substring(model.Packageeditimage.IndexOf(',') + 1);
                    byte[] imageByte = Convert.FromBase64String(imageString);
                    string imageName = Guid.NewGuid().ToString() + ".jpeg";
                    var imgFilePath = Server.MapPath("~/Files/PackageImage/" + imageName);
                    var fileSave = "/Files/PackageImage/" + imageName;

                    using (var imageFile = new FileStream(imgFilePath, FileMode.Create))
                    {
                        imageFile.Write(imageByte, 0, imageByte.Length);
                        imageFile.Flush();
                        cls.Files = fileSave;
                    }
                }

                status = _Entities.SaveChanges() > 0 ? true : false;
                msg = status ? "Package Updated successfully!" : "Failed to Updated Package!";
            }
            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult ViewPackage(long packageid)
        {
            var packages = _Entities.tb_Package.Where(z => z.PackageID == packageid && z.Isactive).FirstOrDefault();
            var model = new Medacademy.Models.PackageModel();
            model.PackageID = packages.PackageID;
            model.CourseID = packages.CourseID;
            model.PackageName = packages.Name;
            model.Type = packages.Type;
            model.Amount = packages.Amount;
            model.DiscountAmount = Convert.ToDecimal(packages.DiscountAmount);
            model.ExpiryDays = packages.ExpiryDays;
            model.Description = packages.Description;
            model.Packageimage = packages.Files;
            model.Isactive = packages.Isactive;

            return PartialView("~/Views/Admin/_pv_Package_View.cshtml", model);
        }

        #endregion

        #region Group
        public ActionResult Group()
        {
            return View();
        }

        public ActionResult AddGroup()
        {
            return View();
        }

        public object AddGroups(GroupModel model)
        {
            bool status = false;
            string msg = string.Empty;
            //if (ModelState.IsValid)
            //{
            var Groups = _Entities.tb_Group.Where(z => z.Groupname.Trim() == model.GroupName.Trim() && z.Isactive).ToList();
            if (Groups.Count == 0)
            {
                var cls = new tb_Group();
                cls.Course_ID = model.CourseID;
                cls.Groupname = model.GroupName;
                cls.PackageID = model.PackageID;
                cls.Isactive = true;
                cls.Timestamp = CurrentTime;
                _Entities.tb_Group.Add(cls);
                status = _Entities.SaveChanges() > 0;
                msg = status ? "Group added successfully!" : "Failed to add Group!";
            }
            else
            {
                status = false;
                msg = "Group already exists!";
            }
            //}
            //else
            //{
            //    status = false;
            //    msg = "Failed to add Group!";
            //}
            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult RefreshGroup()
        {
            return PartialView("~/Views/Admin/_pv_Group_Grid.cshtml", new Medacademy.Models.GroupModel());
        }


        public object EditGroup(GroupModel model)
        {
            bool status = false;
            string msg = string.Empty;
            var cls = _Entities.tb_Group.FirstOrDefault(z => z.Group_ID == model.GroupID && z.Isactive);
            if (cls != null)
            {

                cls.Course_ID = model.CourseID;
                cls.Groupname = model.GroupName;
                cls.PackageID = model.PackageID;

                status = _Entities.SaveChanges() > 0 ? true : false;
                msg = status ? "Group Updated successfully!" : "Failed to Updated Group!";
            }
            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        public object GroupDelete(long groupid)
        {
            bool status = false;
            string msg = string.Empty;
            var groups = _Entities.tb_Group.Where(z => z.Group_ID == groupid && z.Isactive).FirstOrDefault();
            if (groups != null)
            {
                groups.Isactive = false;
                status = _Entities.SaveChanges() > 0;
                msg = status ? " Group Deleted!" : "Failed to delete!";
            }
            else
            {
                status = false;
                msg = "Failed to delete!";
            }
            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        public object Getpackages(long courseid)
        {
            var result = Ma.EntityLibrary.Data.Dropdowndata.GetPackageDrop(courseid);
            return Json(new { status = result.Count > 0, list = result }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult EditGroupView(long groupid)
        {
            var groups = _Entities.tb_Group.Where(z => z.Group_ID == groupid && z.Isactive).FirstOrDefault();
            var model = new Medacademy.Models.GroupModel();
            //model.PackageID = (long?)groups.PackageID;
            //model.CourseID = groups.Course_ID;
            model.GroupName = groups.Groupname;
            model.GroupID = groups.Group_ID;
            return PartialView("~/Views/Admin/_pv_Group_EditGroup.cshtml", model);
        }

        #endregion

        #region Subject
        public ActionResult Subject()
        {
            return View();
        }
        public ActionResult AddSubject()
        {
            return View();
        }

        public object Getgroup(long packageid)
        {
            var result = Ma.EntityLibrary.Data.Dropdowndata.GetGroupDrop(packageid);
            return Json(new { status = result.Count > 0, list = result }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult RefreshSubject()
        {
            return PartialView("~/Views/Admin/_pv_Subject_Grid.cshtml", new Medacademy.Models.SubjectModel());
        }

        public object AddSubjects(SubjectModel model)
        {
            bool status = false;
            string msg = string.Empty;
            //if (ModelState.IsValid)
            //{

            var cls = new tb_Subjects();
            cls.GroupID = model.GroupID;
            cls.SubjectName = model.SubjectName;
            cls.PackageID = model.PackageID;
            cls.Isactive = true;
            cls.Timestamp = CurrentTime;

            _Entities.tb_Subjects.Add(cls);
            status = _Entities.SaveChanges() > 0;
            msg = status ? "Subject added successfully!" : "Failed to add Subject!";

            //}
            //else
            //{
            //    status = false;
            //    msg = "Failed to add Subject!";
            //}
            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult EditSubjectView(long subjectid)
        {
            var groups = _Entities.tb_Subjects.Where(z => z.SubjectID == subjectid && z.Isactive).FirstOrDefault();
            var model = new Medacademy.Models.SubjectModel();
            model.SubjectID = groups.SubjectID;
            model.PackageID = groups.PackageID;
            //model.GroupID = groups.GroupID;
            model.SubjectName = groups.SubjectName;
            return PartialView("~/Views/Admin/_pv_Subject_EditSubject.cshtml", model);
        }

        public object EditSubject(SubjectModel model)
        {
            bool status = false;
            string msg = string.Empty;
            var cls = _Entities.tb_Subjects.FirstOrDefault(z => z.SubjectID == model.SubjectID && z.Isactive);
            if (cls != null)
            {

                cls.GroupID = model.GroupID;
                cls.SubjectName = model.SubjectName;
                cls.PackageID = model.PackageID;

                status = _Entities.SaveChanges() > 0 ? true : false;
                msg = status ? "Subject Updated successfully!" : "Failed to Updated Subject!";
            }
            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        public object SubjectDelete(long subjectid)
        {
            bool status = false;
            string msg = string.Empty;
            var subjects = _Entities.tb_Subjects.Where(z => z.SubjectID == subjectid && z.Isactive).FirstOrDefault();
            if (subjects != null)
            {
                subjects.Isactive = false;
                status = _Entities.SaveChanges() > 0;
                msg = status ? " Subject Deleted!" : "Failed to delete!";
            }
            else
            {
                status = false;
                msg = "Failed to delete!";
            }
            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Topic
        public ActionResult Topic()
        {
            return View();
        }

        public ActionResult AddTopic()
        {
            return View();
        }

        public object AddTopics(TopicModel model)
        {
            bool status = false;
            string msg = string.Empty;

            var cls = new tb_Topic();
            cls.SubjectID = model.SubjectID;
            cls.TopicName = model.TopicName;
            cls.IsActive = true;
            cls.Timestamp = CurrentTime;

            _Entities.tb_Topic.Add(cls);
            status = _Entities.SaveChanges() > 0;
            msg = status ? "Topic added successfully!" : "Failed to add Topic!";

            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult RefreshTopic()
        {
            return PartialView("~/Views/Admin/_pv_Topic_Grid.cshtml", new Medacademy.Models.TopicModel());
        }

        public PartialViewResult EditTopicView(long topicid)
        {
            var topic = _Entities.tb_Topic.Where(z => z.TopicID == topicid && z.IsActive).FirstOrDefault();
            var model = new Medacademy.Models.TopicModel();
            model.SubjectID = topic.SubjectID;
            model.TopicID = topic.TopicID;
            //model.GroupID = groups.GroupID;
            model.TopicName = topic.TopicName;
            return PartialView("~/Views/Admin/_pv_Topic_EditTopic.cshtml", model);
        }

        public object EditTopic(TopicModel model)
        {
            bool status = false;
            string msg = string.Empty;
            var cls = _Entities.tb_Topic.FirstOrDefault(z => z.TopicID == model.TopicID && z.IsActive);
            if (cls != null)
            {

                cls.SubjectID = model.SubjectID;
                cls.TopicName = model.TopicName;
                //cls.PackageID = model.PackageID;

                status = _Entities.SaveChanges() > 0 ? true : false;
                msg = status ? "Topic Updated successfully!" : "Failed to Updated Topic!";
            }
            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        public object TopicDelete(long topicid)
        {
            bool status = false;
            string msg = string.Empty;
            var topics = _Entities.tb_Topic.Where(z => z.TopicID == topicid && z.IsActive).FirstOrDefault();
            if (topics != null)
            {
                topics.IsActive = false;
                status = _Entities.SaveChanges() > 0;
                msg = status ? " Topic Deleted!" : "Failed to delete!";
            }
            else
            {
                status = false;
                msg = "Failed to delete!";
            }
            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region SubTopoic

        public ActionResult SubTopic()
        {
            return View();
        }

        public ActionResult AddSubTopic()
        {
            SubTopicModel model = new SubTopicModel();
            List<SubTopicModel> videolist = new List<SubTopicModel>();

            var getSubtopic = _Entities.tb_Subtopic.Max(x => x.SubTopicID);
            long subtopicNum = getSubtopic;
            subtopicNum = subtopicNum + 1;
            string subNum = subtopicNum.ToString();

            var a1 = _Entities.tb_VideoFiles.Where(x => x.IsActive == true && x.SubTopicID == subNum).ToList();
            if (a1.Count != 0)
            {
                foreach (var a2 in a1)
                {
                    SubTopicModel video = new SubTopicModel();
                    video.ID = a2.ID;
                    video.Name = a2.Name;
                    video.FileSize = a2.FileSize;
                    video.FilePath = a2.FilePath;
                    videolist.Add(video);
                }
                model.SubTopicModel_List = videolist;

                return View(model);
            }

            return View(model);
            

        }

        [HttpPost]
        public ActionResult UploadVideo(HttpPostedFileBase fileupload)
        {
            if (fileupload != null)
            {
                string dirFullPath = HttpContext.Server.MapPath("~/VideoFileUpload/");
                if (!Directory.Exists(dirFullPath))
                {
                    Directory.CreateDirectory(dirFullPath);
                }

                var getSubtopic = _Entities.tb_Subtopic.Max(x => x.SubTopicID);
                long subtopicNum = getSubtopic;
                subtopicNum = subtopicNum + 1;
                string subNum = subtopicNum.ToString();
                var searchFile = _Entities.tb_VideoFiles.Where(x => x.SubTopicID == subNum && x.IsActive == true).ToList();
                foreach (var a1 in searchFile)
                {
                    string rootFolder = HttpContext.Server.MapPath("~/VideoFileUpload/");
                    string authorsFile = a1.Name;
                    string fullPath = rootFolder + authorsFile;
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);                        
                    }
                    var updateVideofileDB = _Entities.tb_VideoFiles.Where(x => x.ID == a1.ID && x.IsActive == true).FirstOrDefault();
                    if(updateVideofileDB != null)
                    {
                        updateVideofileDB.IsActive = false;
                        _Entities.SaveChanges();
                    }                   
                    rootFolder = string.Empty;
                    authorsFile = string.Empty;
                    fullPath = string.Empty;
                }

                //string fileName = Path.GetFileName(fileupload.FileName);
                string Extention = Path.GetExtension(fileupload.FileName);
                string fileName = string.Format("{0:yyyy-MM-dd_hh-mm-ss-tt}"+ Extention, DateTime.Now);


                int fileSize = fileupload.ContentLength;
                int Size = fileSize / 1000;
                fileupload.SaveAs(Server.MapPath("~/VideoFileUpload/" + fileName));

                var create = _Entities.tb_VideoFiles.Create();
                create.Name = fileName;
                create.FileSize = Size;
                create.IsActive = true;
                create.SubTopicID = subtopicNum.ToString();
                create.FilePath = "~/VideoFileUpload/" + fileName;
                _Entities.tb_VideoFiles.Add(create);
                _Entities.SaveChanges();

                Session["UploadFilesName"] = "~/VideoFileUpload/" + fileName;

            }
            return RedirectToAction("AddSubTopic");
        }



        public JsonResult RemoveVideoFiles(SubTopicModel model)
        {
            var dats = _Entities.tb_VideoFiles.Where(x => x.ID == model.ID).FirstOrDefault();
            dats.IsActive = false;
            _Entities.SaveChanges();

            string rootFolder = HttpContext.Server.MapPath("~/VideoFileUpload/");
            string authorsFile = dats.Name;
            string fullPath = rootFolder + authorsFile;
            try
            {
                  
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                    return Json("Your imaginary file has been deleted.", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("File not found", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

            
        }

        public JsonResult RemoveSubtopic(SubTopicModel model)
        {
            string id = model.ID.ToString();
            var dats = _Entities.tb_VideoFiles.Where(x => x.SubTopicID == id).FirstOrDefault();
            var datas1 = _Entities.tb_Subtopic.Where(x => x.SubTopicID == model.ID).FirstOrDefault();
            datas1.Isactive = false;
            dats.IsActive = false;
            _Entities.SaveChanges();

            string rootFolder = HttpContext.Server.MapPath("~/VideoFileUpload/");
            string authorsFile = dats.Name;
            string fullPath = rootFolder + authorsFile;
            try
            {

                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                    return Json("Your imaginary file has been deleted.", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("File not found", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }


        }

        public PartialViewResult RefreshSubTopic()
        {
            return PartialView("~/Views/Admin/_pv_SubTopic_Grid.cshtml", new Medacademy.Models.SubTopicModel());
        }

        [HttpPost]
        public ActionResult EditUploadVideo(HttpPostedFileBase fileupload)
        {
            long passPara = 0;
            passPara = Convert.ToInt64(Session["EditSubTopic"]);
            string subNum = Session["EditSubTopic"].ToString();

            if (fileupload != null)
            {
               
                
                string Extention = Path.GetExtension(fileupload.FileName);
                string fileName = string.Format("{0:yyyy-MM-dd_hh-mm-ss-tt}" + Extention, DateTime.Now);


                int fileSize = fileupload.ContentLength;
                int Size = fileSize / 1000;
                fileupload.SaveAs(Server.MapPath("~/VideoFileUpload/" + fileName));

                var create = _Entities.tb_VideoFiles.Where(x => x.IsActive == true && x.SubTopicID == subNum).FirstOrDefault(); ;

                string rootFolder = HttpContext.Server.MapPath("~/VideoFileUpload/");
                string authorsFile = create.Name;
                string fullPath = rootFolder + authorsFile;
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                    
                }

                create.Name = fileName;
                create.FileSize = Size; 
                create.FilePath = "~/VideoFileUpload/" + fileName;                
                _Entities.SaveChanges();

                Session["UploadFilesName"] = "~/VideoFileUpload/" + fileName;

            }
            return RedirectToAction("EditSubTopic", "Admin", new { topicid = passPara });
            
        }
        public ActionResult EditSubTopic(long topicid)
        {
            Session["EditSubTopic"] = topicid;

            SubTopicModel model = new SubTopicModel();
            List<SubTopicModel> videolist = new List<SubTopicModel>();

            //var getSubtopic = _Entities.tb_Subtopic.Max(x => x.SubTopicID);
            //long subtopicNum = getSubtopic;
            //subtopicNum = subtopicNum + 1;


            string subNum = topicid.ToString();
            var var_subTop = _Entities.tb_Subtopic.Where(x => x.SubTopicID == topicid).FirstOrDefault();
            //var var_Topic = _Entities.tb_Topic.Where(x => x.TopicID == var_subTop.TopicID).FirstOrDefault();
            var a1 = _Entities.tb_VideoFiles.Where(x => x.IsActive == true && x.SubTopicID == subNum).ToList();
            if (a1.Count != 0)
            {
                foreach (var a2 in a1)
                {
                    SubTopicModel video = new SubTopicModel();
                    video.ID = a2.ID;
                    video.Name = a2.Name;
                    video.FileSize = a2.FileSize;
                    video.FilePath = a2.FilePath;
                    videolist.Add(video);
                }
                model.SubTopicID = topicid;
                model.SubTopicModel_List = videolist;
                model.SubTopicName = var_subTop.SubTopicName;
                model.TopicID = var_subTop.TopicID;
                model.TopicID_Edit = var_subTop.TopicID;
                //model.Pdfpath = var_subTop.Pdfpath;
                // model.SelectListItems = Dropdowndata.GetTopicDrop();
            }
                return View(model);
            }

        public object EditAddSubTopics(SubTopicModel model)
        {
            long subtopicId = Convert.ToInt64(Session["EditSubTopic"]);
            
            bool status = false;
            string msg = string.Empty;
            string subtopicId_str = subtopicId.ToString();

            //var cls = new tb_Subtopic();
            var get_VideoFiles = _Entities.tb_VideoFiles.Where(x => x.IsActive == true && x.SubTopicID == subtopicId_str).FirstOrDefault();
            var cls = _Entities.tb_Subtopic.Where(x => x.Isactive == true && x.SubTopicID == subtopicId).FirstOrDefault();
            if (model.TopicID_Edit != null)
            {
                cls.TopicID = model.TopicID_Edit;
            }
            //cls.TopicID = model.TopicID;
            cls.SubTopicName = model.SubTopicName;
            //cls.Isactive = true;
            cls.Timestamp = CurrentTime;
            cls.Videopath = get_VideoFiles.FilePath;

            //pdf upload

            if (model.Pdfpath != null)
            {
                string folderPath = Server.MapPath("~/Files/PDF/");
                if (!Directory.Exists(folderPath))
                    Directory.CreateDirectory(folderPath);
                var imageString = model.Pdfpath.Substring(model.Pdfpath.IndexOf(',') + 1);
                byte[] imageByte = Convert.FromBase64String(imageString);
                string imageName = Guid.NewGuid().ToString() + ".pdf";
                var imgFilePath = Server.MapPath("~/Files/PDF/" + imageName);
                var fileSave = "/Files/PDF/" + imageName;

                using (var imageFile = new FileStream(imgFilePath, FileMode.Create))
                {
                    imageFile.Write(imageByte, 0, imageByte.Length);
                    imageFile.Flush();
                    cls.Pdfpath = fileSave;
                }
            }
                         
            status = _Entities.SaveChanges() > 0;
            msg = status ? "Subtopic Update successfully!" : "Failed to Update Subtopic!";

            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult EditSubTopicView(long topicid)
        {


            SubTopicModel model = new SubTopicModel();
            List<SubTopicModel> videolist = new List<SubTopicModel>();

            //var getSubtopic = _Entities.tb_Subtopic.Max(x => x.SubTopicID);
            //long subtopicNum = getSubtopic;
            //subtopicNum = subtopicNum + 1;


            string subNum = topicid.ToString();
            var var_subTop = _Entities.tb_Subtopic.Where(x => x.SubTopicID == topicid).FirstOrDefault();
            var var_Topic = _Entities.tb_Topic.Where(x => x.TopicID == var_subTop.TopicID).FirstOrDefault();
            var a1 = _Entities.tb_VideoFiles.Where(x => x.IsActive == true && x.SubTopicID == subNum).ToList();
            if (a1.Count != 0)
            {
                foreach (var a2 in a1)
                {
                    SubTopicModel video = new SubTopicModel();
                    video.ID = a2.ID;
                    video.Name = a2.Name;
                    video.FileSize = a2.FileSize;
                    video.FilePath = a2.FilePath;
                    videolist.Add(video);
                }
                model.SubTopicModel_List = videolist;
                model.SubTopicName = var_subTop.SubTopicName;
                model.TopicID = var_Topic.TopicID;
                model.Pdfpath = var_subTop.Pdfpath;
                model.SelectListItems = Dropdowndata.GetTopicDrop();
                //return View(model);
                return PartialView("~/Views/Admin/_pv_SubTopic_EditTopic.cshtml", model);
            }

            //return View(model);


            //var topic = _Entities.tb_Topic.Where(z => z.TopicID == topicid && z.IsActive).FirstOrDefault();
            //var model = new Medacademy.Models.TopicModel();
            //model.SubjectID = topic.SubjectID;
            //model.TopicID = topic.TopicID;
            ////model.GroupID = groups.GroupID;
            //model.TopicName = topic.TopicName;
            return PartialView("~/Views/Admin/_pv_SubTopic_EditTopic.cshtml", model);
            //return PartialView("~/Views/Admin/_pv_SubTopic_EditTopic.cshtml");

        }

        public object AddSubTopics(SubTopicModel model)
        {
            string videoFiles = Session["UploadFilesName"].ToString();
            if (videoFiles == string.Empty)
            {
                videoFiles = null;
            }

            bool status = false;
            string msg = string.Empty;

            var cls = new tb_Subtopic();
            cls.TopicID = model.TopicID;
            cls.SubTopicName = model.SubTopicName;
            cls.Isactive = true;
            cls.Timestamp = CurrentTime;
            cls.Videopath = videoFiles;

            //pdf upload

            if (model.Pdfpath != null)
            {
                string folderPath = Server.MapPath("~/Files/PDF/");
                if (!Directory.Exists(folderPath))
                    Directory.CreateDirectory(folderPath);
                var imageString = model.Pdfpath.Substring(model.Pdfpath.IndexOf(',') + 1);
                byte[] imageByte = Convert.FromBase64String(imageString);
                string imageName = Guid.NewGuid().ToString() + ".pdf";
                var imgFilePath = Server.MapPath("~/Files/PDF/" + imageName);
                var fileSave = "/Files/PDF/" + imageName;

                using (var imageFile = new FileStream(imgFilePath, FileMode.Create))
                {
                    imageFile.Write(imageByte, 0, imageByte.Length);
                    imageFile.Flush();
                    cls.Pdfpath = fileSave;
                }
            }
            if (model.Videopath != null)
            {
                string folderPath = Server.MapPath("~/Files/Video/");
                if (!Directory.Exists(folderPath))
                    Directory.CreateDirectory(folderPath);
                var imageString = model.Videopath.Substring(model.Videopath.IndexOf(',') + 1);
                byte[] imageByte = Convert.FromBase64String(imageString);
                string imageName = Guid.NewGuid().ToString() + model.extension;
                var imgFilePath = Server.MapPath("~/Files/Video/" + imageName);
                var fileSave = "/Files/Video/" + imageName;

                using (var imageFile = new FileStream(imgFilePath, FileMode.Create))
                {
                    imageFile.Write(imageByte, 0, imageByte.Length);
                    imageFile.Flush();
                    cls.Videopath = fileSave;
                }


            }


            _Entities.tb_Subtopic.Add(cls);
            status = _Entities.SaveChanges() > 0;
            msg = status ? "Subtopic added successfully!" : "Failed to add Subtopic!";

            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region Level
        public ActionResult Level()
        {
            return View();
        }
        public ActionResult AddLevel()
        {
            return View();
        }

        public object AddLevels(LevelsModel model)
        {
            bool status = false;
            string msg = string.Empty;

            var cls = new tb_Level();
            cls.SubTopicID = model.SubTopicID;
            cls.LevelName = model.LevelName;
            cls.IsActive = true;
            cls.Timestamp = CurrentTime;

            _Entities.tb_Level.Add(cls);
            status = _Entities.SaveChanges() > 0;
            msg = status ? "Level added successfully!" : "Failed to add Level!";

            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult RefreshLevel()
        {
            return PartialView("~/Views/Admin/_pv_Level_Grid.cshtml", new Medacademy.Models.LevelsModel());
        }

        public PartialViewResult EditLevelView(long levelid)
        {
            var topic = _Entities.tb_Level.Where(z => z.LevelID == levelid && z.IsActive).FirstOrDefault();
            var model = new Medacademy.Models.LevelsModel();
            model.SubTopicID = topic.SubTopicID;
            model.LevelID = topic.LevelID;
            //model.GroupID = groups.GroupID;
            model.LevelName = topic.LevelName;
            return PartialView("~/Views/Admin/_pv_Level_EditTopic.cshtml", model);
        }

        public object EditLevel(LevelsModel model)
        {
            bool status = false;
            string msg = string.Empty;
            var cls = _Entities.tb_Level.FirstOrDefault(z => z.LevelID == model.LevelID && z.IsActive);
            if (cls != null)
            {

                cls.SubTopicID = model.SubTopicID;
                cls.LevelName = model.LevelName;
                //cls.PackageID = model.PackageID;

                status = _Entities.SaveChanges() > 0 ? true : false;
                msg = status ? "Topic Updated successfully!" : "Failed to Updated Topic!";
            }
            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        public object LevelDelete(long levelid)
        {
            bool status = false;
            string msg = string.Empty;
            var topics = _Entities.tb_Level.Where(z => z.LevelID == levelid && z.IsActive).FirstOrDefault();
            if (topics != null)
            {
                topics.IsActive = false;
                status = _Entities.SaveChanges() > 0;
                msg = status ? " Topic Deleted!" : "Failed to delete!";
            }
            else
            {
                status = false;
                msg = "Failed to delete!";
            }
            return Json(new { status = status, msg = msg }, JsonRequestBehavior.AllowGet);
        }


        #endregion



    }
}