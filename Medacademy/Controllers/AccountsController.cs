﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medacademy.Models;
using Ma.ClassLibrary;
using Ma.EntityLibrary;


namespace Medacademy.Controllers
{
    public class AccountsController : Controller
    {
        public DateTime CurrentTime = TimeZoneInfo.ConvertTimeFromUtc(System.DateTime.Now.ToUniversalTime(), TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
        public MAEntities _Entities = new MAEntities();
        // GET: Accounts
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Courses()
        {
            return View();
        }
        public ActionResult Packages()
        {
            return View();
        }
        public ActionResult Preparation()
        {
            return View();
        }
        public ActionResult Reports()
        {
            return View();
        }
        public ActionResult Groups()
        {
            return View();
        }
        public ActionResult AboutUs()
        {
            return View();
        }
        public ActionResult ContactUs()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult SignUp()
        {
            return View();
        }
        public ActionResult LoginCheck(Login model)
        {
            try
            {
                var user = _Entities.tb_Login.Where(x => x.Email.ToLower() == model.Email.ToLower() && x.Password == model.Password && x.IsActive == true).FirstOrDefault();
                if (user != null)
                {

                    if (user.RoleId == 1)
                    {

                        return Json(new { status = true, msg = "Success", userType = 1 }, JsonRequestBehavior.AllowGet);
                    }
                    else if (user.RoleId == 2)
                    {

                        return Json(new { status = true, msg = "Success", userType = 2 }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { status = false, msg = "Username/Password does not match" }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    return Json(new { status = false, msg = "Username/Password incorrect" }, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception ex)
            {
                return Json(new { status = false, msg = ex.InnerException.InnerException }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}