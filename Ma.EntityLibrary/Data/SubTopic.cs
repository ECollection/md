﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ma.EntityLibrary.Data
{
   public class SubTopic : BaseReference
    {
        private tb_Subtopic SubTopics;
        public SubTopic() { }
        public SubTopic(tb_Subtopic crs) { SubTopics = crs; }
        public SubTopic(long subtopicid) { SubTopics = _Entities.tb_Subtopic.FirstOrDefault(x => x.SubTopicID == SubTopicID); }
        public long SubTopicID { get { return SubTopics.SubTopicID; } }

        public long TopicID { get { return SubTopics.TopicID; } }

        public string SubTopicName { get { return SubTopics.SubTopicName; } }

        public string Pdfpath { get { return SubTopics.Pdfpath; } }
        public string Videopath { get { return SubTopics.Videopath; } }
        public List<SubTopic> GetTopics()
        {
            return _Entities.tb_Subtopic.Where(x => x.Isactive == true).ToList().Select(q => new SubTopic(q)).OrderBy(c => c.SubTopicID).ToList();
        }
    }
}
