﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ma.EntityLibrary.Data
{
    public class Level : BaseReference
    {
        private tb_Level Levels;
        public Level() { }
        public Level(tb_Level crs) { Levels = crs; }
        public Level(long levelid) { Levels = _Entities.tb_Level.FirstOrDefault(x => x.LevelID == levelid); }


        public long LevelID { get { return Levels.LevelID; } }
        public long SubTopicID { get { return Levels.SubTopicID; } }
        public string LevelName { get { return Levels.LevelName; } }
        public System.DateTime Timestamp { get { return Levels.Timestamp; } }

        public List<Level> GetLevels()
        {
            return _Entities.tb_Level.Where(x => x.IsActive == true).ToList().Select(q => new Level(q)).OrderBy(c => c.LevelID).ToList();
        }
        public List<Question> GetQuestions()
        {
            return _Entities.tb_Question.Where(z => z.LevelID == Levels.LevelID && z.IsActive).ToList().Select(q => new Question(q)).ToList();
        }
    }
}
