//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ma.EntityLibrary
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_Subtopic
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tb_Subtopic()
        {
            this.tb_Level = new HashSet<tb_Level>();
        }
    
        public long SubTopicID { get; set; }
        public long TopicID { get; set; }
        public string SubTopicName { get; set; }
        public bool Isactive { get; set; }
        public System.DateTime Timestamp { get; set; }
        public string Pdfpath { get; set; }
        public string Videopath { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tb_Level> tb_Level { get; set; }
        public virtual tb_Topic tb_Topic { get; set; }
    }
}
